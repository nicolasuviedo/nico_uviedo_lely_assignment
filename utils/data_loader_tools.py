from os import listdir
from os.path import isfile, join
from typing import List, Dict, Optional
import datetime

import pandas as pd

class DataLoader:
    
    def __init__(self,specs):
        DataLoader.validate(specs)
        self.specs = specs
    
    def load_latest_from_source(self, store_latest=True, **kwargs):
        data = self.specs['data_getter'](**kwargs) #asumes pandas dataframe as output
        version = self.specs['version_getter'](**kwargs)
        
        if store_latest:
            data.to_csv(f"data/static_data/{self.specs['technical_name']}.{version}.csv")
            
        return data, version
    
    def load_latest_from_local(self):
        technical_name = self.specs['technical_name']
        version = self.specs['version_getter']()
        path = f'data/static_data/{technical_name}.{version}.csv'
        data = pd.read_csv(path,index_col='id')
        
        return data, version
    
    
    def load_lastest(self, try_local_first=False):
        
        if try_local_first:
            try:
                return self.load_latest_from_local()
            except:
                return self.load_latest_from_source(store_latest=True)
        else:
            try:
                return self.load_latest_from_source(store_latest=True)
            except:
                return self.load_latest_from_local()
    
    
    def validate(specs):
        
        required_fields = ['technical_name',
                           'data_getter',
                           'version_getter']
        for e in required_fields:
            try:
                specs[e]
            except:
                raise Exception(f"field {e} is missing in the dictionary")