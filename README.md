# FOR READING THE CONTENT:
0 - open summary.ipynb

1 - open exploration.ipynb


# FOR RUNNING THE CODE, AND EXPLORING THE RESULTS:
0 - on cmd:```pip install -r requirements.txt```

1 - open and run all cells: exploration.ipynb (20 minutes end to end)

2 - on cmd: mlflow server

3 - go to mlflow ui (default: http://127.0.0.1:5000)


# NAVIGATING THIS REPO
- data/ : has the data needed for the project, also metadata

- images/ : has some of the plots stored during the analysis
- mlruns/ : is the project forlder for mlflow, it contains the artifacts of all models trained
- model_artifacts/ : has the final models
- research/ : has files and links to some of the sources used for researching
- unused/ : has pieces of code that were not included in the final delivery
- utils/ : has some tools I created to simplify some processes
- exploration_comments_artifact.pkl : has documented the observations and asummptions developed during the analysis
- requirements.txt : has the used libraries to run this project (without versions)