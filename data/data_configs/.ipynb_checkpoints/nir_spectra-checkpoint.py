import pandas as pd

def data_getter():
    url = 'https://github.com/datamole-ai/NirSpectra/blob/master/cal%202018.csv.zip' 

    df = pd.read_csv(url,
                 sep=';',
                 header=None)

    df.columns = ['id','target'] + list(df.columns)[2:]
    df.set_index('id',drop=True,inplace=True)

    return df
    
specs = {
    'technical_name': 'nir_spectra',
    'data_getter': data_getter ,
    'version_getter': lambda: '01.00.00' #this data is not versioned, it will remain 01.00.00
    
}

