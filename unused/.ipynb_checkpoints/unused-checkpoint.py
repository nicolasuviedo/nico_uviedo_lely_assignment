#finding patterns in the cummulative distribution
_support_vec['ones']=1
_support_vec = _support_vec.sort_values('target').set_index('target',drop=True)
_support_vec = _support_vec.cumsum()/_support_vec.shape[0]
_support_vec = _support_vec.rename(columns={'ones':'c_sum'})
_support_vec = _support_vec.groupby('target').max()
sns.lineplot(_support_vec.index, _support_vec.c_sum)